source 'https://rubygems.org'

gem 'rails', '3.2.22.5'
gem 'rake', '11.3.0'

gem 'syck'
gem 'test-unit'
gem 'minitest'

gem "prawn"
gem "mongo"
gem "mongoid", "3.1.7"
gem 'savon', '~> 2.8.0'
gem 'net-http-persistent', "2.9.4"
gem 'net-sftp'
gem 'nokogiri', '1.6.5'
gem 'bcrypt-ruby', '3.0.1'
gem "faraday", '0.9.1'
gem "rubyntlm", '0.4.0'
gem 'parse-cron'
gem 'mongoid-multitenancy'
gem "mongoid-paperclip", :require => "mongoid_paperclip"
gem 'mongoid_auto_increment'
gem "mongo_session_store-rails3"
gem 'aws-sdk-v1', '1.66.0'
gem 'aws-sdk', '~> 2'
gem 'connection_pool'
gem 'rails_admin'
gem 'bootstrap-will_paginate'
gem 'will_paginate_mongoid'
gem "figaro"
gem 'addressable'
gem 'ruby-prof'
gem 'correios-frete'
gem 'money-rails'
gem "bullet", :group => "development"
gem 'simple_form'
gem 'amazon-ses-mailer', "0.0.4"
gem 'htmlentities'
gem 'httpclient'
gem 'rails-html-sanitizer'
gem 'logstash-logger'
gem 'scout_apm', '~> 2.1.22'
gem 'awesome_print', '1.6.1'
gem 'rack-cors', '~> 0.4.1', :require => 'rack/cors'

#ELASTICSEARCH
gem 'elasticsearch-persistence', require: 'elasticsearch/persistence/model'

#REDIS SESSION STORAGE
gem 'redis-rails'
gem "hiredis", "~> 0.6.0"

#SIDEKID
source "https://4e2cd59f:8468e9b1@gems.contribsys.com/" do
  gem 'sidekiq-pro', '2.0.5'
end
gem 'sidekiq', '3.5.3'
gem 'sinatra', :require => nil
gem "sidekiq-cron", '0.3.1'
gem 'sidekiq-unique-jobs'
gem "kiqstand"
gem 'honeybadger', '~> 2.0'

gem 'oauth2'
gem 'legato'
gem 'turbolinks', '2.2.0'
gem 'jquery-turbolinks'
gem 'nprogress-rails'

gem "strong_parameters"
gem 'newrelic_rpm'

gem 'jquery-rails'
gem 'jquery-ui-rails'
gem "curb", "~> 0.8.8"

gem 'turnout'

gem "select2-rails", '3.5.9.1'

gem 'swagger-docs'

gem 'axlsx', '~> 2.0.1'

group :test, :development do
  gem 'orderly'
  gem "rspec-rails", "~> 2.99.0"
  gem "factory_girl_rails", "~> 4.0"
  gem 'byebug', '8.2.0'
  gem 'execjs'
  gem 'therubyracer'
  gem 'parallel_tests'
end

group :development do
  gem 'rack-mini-profiler'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
end

group :test do
  gem 'simplecov', :require => false
  gem 'capybara', '1.1.2'
  gem 'capybara-select2'
  gem 'vcr'
  gem 'webmock'
  gem 'selenium-webdriver', '>= 2.49.0'
  gem 'timecop'
  gem 'rspec-html-matchers'
  gem 'poltergeist'
  gem "mock_redis"
end

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
end

gem 'rack', '1.4.7'
gem "font-awesome-rails"
gem "devise", "3.5.3"
gem "jwt", "0.1.11"
gem "cancan"
